# noxus

#### Description
Nexus is a mighty empire. 
In the eyes of people outside Nexus, it is self-respecting, bloody and barbaric, but for those who see through its belligerent appearance, the social atmosphere here is actually unusually tolerant. 
All the talents and talents of the people will be respected and nurtured. 
The ancient Knoxies were an alliance of brutal barbarian tribes who occupied an ancient city and built it into the center of what is now the empire. 
At that time, Noxus was faced with threats from all sides, so they fought fiercely with all their enemies, vengeance, no return, and finally allowed the empire to expand year after year. 
This difficult history of survival makes the people of Noxus proud from the bottom of their bones and therefore values power above all else. 
Of course, power can be expressed in many different forms. 
Regardless of social stand, background, homeland and personal wealth, anyone can gain power, status, and respect in Nexus, as long as they can demonstrate the necessary abilities. 
People who can use magic will be looked down upon, and the empire will even take the initiative to look for such people to exercise their special talents and use them most efficiently. 
Although Noxus has the political ideal of virtuous rule, the older generation of aristocratic families still hold considerable power in the heart of the empire, and some people worry that Noxus's greatest threat comes not from enemies, but from within.

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request

